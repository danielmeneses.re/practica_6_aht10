/*
 * aht10.h
 *
 *  Created on: Nov 25, 2023
 *      Author: Daniel Meneses
 */

#ifndef INC_AHT10_H_
#define INC_AHT10_H_

#include "i2c.h"

#define AHT10_ADDR 0x38<<1

enum{
	AHT10initCMD = 0xE1,
	AHT10triggerCMD = 0xAC,
	AHT10resetCMD = 0xBA
};

typedef union
{
	struct
	{
		uint8_t dumy 		:3;
		uint8_t CalEn 		:1;
		uint8_t reserved 	:1;
		uint8_t WorkMode 	:2;
		uint8_t Bussy 		:1;
	}StatusBits;
	uint8_t StatusWord;
}aht10_status_t;

typedef struct
{
	float humidity;
	float TemperatureC;
}aht10_sensorData_t;

void aht10_sensorInit(void);
void aht10_sensorTrigger(void);
void aht10_sensorReset(void);
void aht10_sensorGetData(aht10_sensorData_t *data);
#endif /* INC_AHT10_H_ */
