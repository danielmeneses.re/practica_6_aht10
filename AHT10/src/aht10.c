/*
 * aht10.c
 *
 *  Created on: Nov 25, 2023
 *      Author: Daniel Meneses
 */

#include "aht10.h"
#include <stdbool.h>

const uint32_t aht10Factor = 1048576; /* 2 ^ 20   ****  1 << 20*/

static void aht10_writeCMD(uint8_t command)
{
	HAL_I2C_Master_Transmit(&hi2c1, AHT10_ADDR, &command, 1, 100);
}

static void aht10_writeExtendedCMD(uint8_t *extCMD)
{
	HAL_I2C_Master_Transmit(&hi2c1, AHT10_ADDR, extCMD, 3, 100);
}

static bool aht10_readData(uint32_t *rawHumidity, uint32_t *rawTemp)
{
	uint8_t sData[6];
	aht10_status_t status;
	bool state = false;
	uint32_t rh_0 = 0, rh_1 = 0, rh_2 = 0;
	uint32_t temp_0 = 0, temp_1 = 0, temp_2 = 0;
	HAL_I2C_Master_Receive(&hi2c1, AHT10_ADDR, sData, 6, 100);

	status.StatusWord = sData[0];
	if (!status.StatusBits.Bussy)
	{
		rh_0 = (sData[3] >> 4) & 0x0F;
		rh_1 = (sData[2] << 4) & 0xFF0;
		rh_2 = (sData[1] << 12) & 0xFF000;
		*rawHumidity = rh_0 + rh_1 + rh_2;

		temp_0 = (sData[3] << 16) & 0xF0000;
		temp_1 = (sData[4] << 8) & 0xFF00;
		temp_2 = sData[5] & 0xFF;

		*rawTemp = temp_0 + temp_1 + temp_2;
		state = true;
	}

	else
	{
		*rawHumidity = 0;
		*rawTemp = 0;
		state = false;
	}

	return state;
}

void aht10_sensorInit(void)
{
	aht10_writeCMD(AHT10initCMD);
}

void aht10_sensorTrigger(void)
{
	uint8_t triggerCMD[3];

	triggerCMD[0] = AHT10triggerCMD;
	triggerCMD[1] = 0x33;
	triggerCMD[2] = 0;
	aht10_writeExtendedCMD(triggerCMD);
}
void aht10_sensorReset(void)
{
	aht10_writeCMD(AHT10resetCMD);
}

void aht10_sensorGetData(aht10_sensorData_t *data)
{
	uint32_t temp, rh;
	if ( aht10_readData(&rh, &temp) )
	{
		data->TemperatureC = (float) (((temp * 200) / aht10Factor) - 50);
		data->humidity = (float) ((rh * 100) / aht10Factor);
	}
	else
	{
		data->TemperatureC = 0;
		data->humidity = 0;
	}
}
